
My notes about docker for ROS.

1. Build the image.

```.sh
#docker build --tag ros:ros-tutorials .
docker build --tag myros .
```

2. Create a newtowrk
```.sh
docker network create rosnet
```

3. Start the master
```.sh
docker run -it --rm --network rosnet --name master myros roscore
```

4. Start the talker
```.sh
docker run -it --rm --network rosnet --name talker \
--env ROS_HOSTNAME=talker \
--env ROS_MASTER_URI=http://master:11311 \
myros \
rosrun roscpp_tutorials talker
```

5. Start the listener
```.sh
docker run -it --rm --network rosnet --name listener \
--env ROS_HOSTNAME=listener \
--env ROS_MASTER_URI=http://master:11311 \
myros \
rosrun roscpp_tutorials listener
```

6. Use docker compose (docker-compose.yml)

```.sh
docker-compose up

docker container prune
```

```.yml
version: '2'
services:
  master:
    build: .
    container_name: master
    command:
      - roscore

  talker:
    depends_on:
      - master
    build: .
    container_name: talker
    environment:
      - ROS_HOSTNAME=talker
      - ROS_MASTER_URI=http://master:11311
    command: rosrun roscpp_tutorials talker

  listener:
    depends_on:
      - master
    build: .
    container_name: listener
    environment:
      - ROS_HOSTNAME=listener
      - ROS_MASTER_URI=http://master:11311
    command: rosrun roscpp_tutorials listener
```

Refer to
 - https://qiita.com/Leonardo-mbc/items/cfd38a4fae8667593cf1
